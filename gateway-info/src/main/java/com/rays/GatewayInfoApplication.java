package com.rays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.rays.item.dao")
public class GatewayInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayInfoApplication.class);
    }
}
