package com.rays.item.util;

/**
* @描述: 响应码
* @Param:
* @return:
* @Date: 2020/10/17
*/
public enum ResponseStatusEnum {

    SUCCESS(200,"success");

    ResponseStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private Integer code;

    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
