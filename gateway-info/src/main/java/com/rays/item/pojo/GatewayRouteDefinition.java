package com.rays.item.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @类描述: Gateway的路由定义模型
 * @author: Mr.Li
 * @create: 2020-10-17 12:32
 * 实现restful接口动态配置路由
 **/
@Table(name = "gateway_info")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GatewayRouteDefinition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//表示自增主键
    private Long id;

    /**
     * 路由的Id
     */
    @Column(name = "route_id")
    private String routeId;

    /**
     * 路由断言集合配置
     */
    @Transient//忽略该属性
    private List<GatewayPredicateDefinition> predicates;

    @Column(name = "predicates_info")
    private String predicatesInfo;

    /**
     * 路由过滤器集合配置
     */
    @Transient//忽略该属性
    private List<GatewayFilterDefinition> filters;

    @Column(name = "filters_info")
    private String filtersInfo;

    /**
     * 路由规则转发的目标uri
     */
    private String uri;

    /**
     * 路由执行的顺序
     */
    @Column(name = "order_info")
    private Integer order;

    /**
     * 路由版本号
     * */
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public List<GatewayPredicateDefinition> getPredicates() {
        return predicates;
    }

    public void setPredicates(List<GatewayPredicateDefinition> predicates) {
        this.predicates = predicates;
    }

    public String getPredicatesInfo() {
        return predicatesInfo;
    }

    public void setPredicatesInfo(String predicatesInfo) {
        this.predicatesInfo = predicatesInfo;
    }

    public List<GatewayFilterDefinition> getFilters() {
        return filters;
    }

    public void setFilters(List<GatewayFilterDefinition> filters) {
        this.filters = filters;
    }

    public String getFiltersInfo() {
        return filtersInfo;
    }

    public void setFiltersInfo(String filtersInfo) {
        this.filtersInfo = filtersInfo;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
