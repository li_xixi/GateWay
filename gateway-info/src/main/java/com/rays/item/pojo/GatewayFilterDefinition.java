package com.rays.item.pojo;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @类描述: 过滤器
 * @author: Mr.Li
 * @create: 2020-10-17 12:33
 **/
public class GatewayFilterDefinition {

    /**
     * Filter Name
     */
    private String name;

    /**
     * 对应的路由规则
     */
    private Map<String, String> args = new LinkedHashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getArgs() {
        return args;
    }

    public void setArgs(Map<String, String> args) {
        this.args = args;
    }
}
