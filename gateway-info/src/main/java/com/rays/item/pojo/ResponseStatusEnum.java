package com.rays.item.pojo;

public enum ResponseStatusEnum {

	RESCODE_0(0, "操作成功"),
	RESCODE_00(0, "指令下发成功"),
	/** *服务器发生异常 */
	RESCODE_1(-1, "服务器发生异常"),
	RESCODE_2(2, "未登录"),
	/** *操作失败! */
	RESCODE_3(3, "无权限"),
	RESCODE_20000(20000, "非法TOKEN"),
	RESCODE_5(5, "非法参数"),
	RESCODE_6(6, "rundeck操作失败"),
	RESCODE_7(7, "持久化失败"),
	RESCODE_8(8, "密码已存在"),
	RESCODE_9(9, "密钥已经存在"),
	RESCODE_10(10, "未获取到HUB TOKEN"),
	RESCODE_11(11, "TOKEN已过期，请重新获取"),

	RESCODE_2_0002_2005(200012005,"qhwz_MsgCommExtern:查询失败"),
	RESCODE_2_0002_2006(200012006,"qhwz_MsgCommExtern:新增失败"),
	RESCODE_2_0002_2007(200012007,"qhwz_MsgCommExtern:修改失败"),
	RESCODE_2_0002_2008(200012008,"qhwz_MsgCommExtern:删除失败"),
	RESCODE_2_0002_2009(200012008,"qhwz_MsgCommExtern:下载文件失败"),
	RESCODE_2_0002_20010(200012008,"qhwz_MsgCommExtern:上传文件失败"),

	//用户信息相关
	RESCODE_1_0001_20010(1000120010,"rays_user:用户不存在");



	/**
	 * 枚举的值
	 * */
	private int code;

	/**
	 * 枚举的中文描述
	 * */
	private String desc;


	ResponseStatusEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}