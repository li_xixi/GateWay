package com.rays.item.pojo;


/**
 * 返回值工具类
 *
 * @author rambler
 * @since 2019年7月2日 18:10:14
 */
public class Response<T> {
    // 状态码,成功为0,失败为1,也封装成常量类
    private Integer status;
    // 消息,成功消息或者失败消息
    private String msg;
    // 要返回的数据
    private T data;
    
    private Integer code;

    public Response(){}

 
    public Integer getStatus() {
        return status;
    }
    
    
 
    public Integer getCode() {
		return code;
	}



	public void setCode(Integer code) {
		this.code = code;
	}



	public void setStatus(Integer status) {
        this.status = status;
    }
 
    public String getMsg() {
        return msg;
    }
 
    public void setMsg(String msg) {
        this.msg = msg;
    }
 
    public T getData() {
        return data;
    }
 
    public void setData(T data) {
        this.data = data;
    }
 
    private Response(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }
 
    private Response(Integer status, String msg, T data, Integer code) {
        this.status = status;
        this.msg = msg;
        this.data = data;
        this.code = code;
    }
 
    public Response(Integer code, String msg, T data) {
    	 this.status = 0;
         this.msg = msg;
         this.code = code;
         this.data = data;
    }
 
    /**
     * 状态码 + 成功提示信息
     */
    public static <T> Response<T> createResponseBySuccess(T data) {
        return new Response<>(ResponseStatusEnum.RESCODE_0.getCode(), ResponseStatusEnum.RESCODE_0.getDesc(),data);
    }
    
    /**
     * 成功返回数据
     * @param msg 返回信息，成功提示信息
     * @param data  返回数据
     * @return
     */
    public static <T> Response<T> createResponseBySuccess(String msg, T data) {
        return new Response<>(ResponseStatusEnum.RESCODE_0.getCode(), msg, data,ResponseStatusEnum.RESCODE_0.getCode());
    }
 
    
    /**
     * 成功返回数据
     * @param msg 返回信息，成功提示信息
     * @param data 返回数据
     * @param code 返回状态码
     * @return 
     */
    public static <T> Response<T> createResponseBySuccess(String msg, T data,Integer code) {
        return new Response<>(ResponseStatusEnum.RESCODE_0.getCode(), msg, data,code);
    }
 
    /**
     * 状态码 + 错误信息
     */
    public static <T> Response<T> createResponseByError(String msg) {
        return new Response<>(ResponseStatusEnum.RESCODE_0.getCode(), msg);
    }
    
    
    /**
     * 错误返回
     * @param msg   错误提示，枚举中的desc
     * @param code  错误码，枚举中的code
     * @return
     */
    public static <T> Response<T> createResponseByError(String msg,Integer code) {
        return new Response<>(code, msg);
    }
}