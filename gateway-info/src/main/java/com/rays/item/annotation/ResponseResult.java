package com.rays.item.annotation;

import java.lang.annotation.*;

/**
 * 实现统一拦截接口，判断返回值是否需要包装
 * */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
public @interface ResponseResult {

}
