package com.rays.item.handler;

import com.rays.item.annotation.ResponseResult;
import com.rays.item.pojo.Response;
import com.rays.item.service.ex.BaseResultException;
import com.rays.item.util.ResponseStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;


@RestControllerAdvice
public class ResponseResultHandler implements ResponseBodyAdvice<Object> {
    private Logger logger = LoggerFactory.getLogger(getClass());

    //标记名称
    private static final String RESPONSE_RESULT_ANN = "RESPONSE-RESULT-ANN";

    @Override
    public boolean supports(MethodParameter returnType, Class aClass) {
        ServletRequestAttributes sqa = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sqa.getRequest();
        //判断请求是否包含我们自定义的包装标记注解
        ResponseResult attribute = (ResponseResult)request.getAttribute(RESPONSE_RESULT_ANN);
        return attribute==null?false:true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (o instanceof BaseResultException){
            logger.info("=====捕获异常，进入返回体，重写异常返回格式处理中-----");
            BaseResultException errorResult = (BaseResultException)o;
            return Response.createResponseByError(errorResult.getMsg(),errorResult.getStatus());
        }else if (o instanceof LinkedHashMap){ LinkedHashMap result = (LinkedHashMap)o;
            logger.error("系统错误...");
            String status =  String.valueOf(result.get("status"));
            String error = String.valueOf(result.get("message"));
            logger.info(result.get("message").toString());
            return Response.createResponseByError(error,Integer.valueOf(status));
        }
        logger.info("=====结果正常，进入返回体，重写返回格式处理中-----");
        return Response.createResponseBySuccess(ResponseStatusEnum.SUCCESS.getDesc(),o,ResponseStatusEnum.SUCCESS.getCode());
    }
}
