package com.rays.item.interceptor;

import com.rays.item.annotation.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class ResponseResultInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = LoggerFactory.getLogger(getClass());

    //标记名称
    private static final String RESPONSE_RESULT_ANN = "RESPONSE-RESULT-ANN";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod){

            final HandlerMethod handlerMethod = (HandlerMethod)handler;
            final Class<?> beanType = handlerMethod.getBeanType();
            final Method method = handlerMethod.getMethod();
            //判断是否在类上添加注解
            if (beanType.isAnnotationPresent(ResponseResult.class)){
                //设置此请求返回体，需要包装，继续向下传递，在ResponseBodyAdvice进行判断
                request.setAttribute(RESPONSE_RESULT_ANN,beanType.getAnnotation(ResponseResult.class));
            }else if (method.isAnnotationPresent(ResponseResult.class)){
                //or判断在方法上添加了注解
                //设置此请求返回体，需要包装，继续向下传递，在ResponseBodyAdvice进行判断
                request.setAttribute(RESPONSE_RESULT_ANN,method.getAnnotation(ResponseResult.class));
            }
        }
        return true;
    }
}