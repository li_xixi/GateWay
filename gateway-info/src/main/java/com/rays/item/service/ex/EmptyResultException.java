package com.rays.item.service.ex;

public class EmptyResultException extends BaseResultException{

    public EmptyResultException(int status, String msg) {
        super(status, msg);
    }
}
