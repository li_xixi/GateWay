package com.rays.item.service;

import com.rays.item.pojo.GatewayRouteDefinition;

import java.util.List;

/**
 * @类描述: 规格参数管理
 * @author: Mr.Li
 * @create: 2020-05-13 21:54
 **/
public interface GatewayRouteService {

    /**
    * @描述: 新增路由
    * @Param:
    * @return:
    * @Date: 2020/10/17
    */
    int add(GatewayRouteDefinition routeDefinition);

    /**
     * @描述: 修改路由
     * @Param:
     * @return:
     * @Date: 2020/10/17
     */
    int update(GatewayRouteDefinition routeDefinition);

    /**
     * @描述: 删除路由
     * @Param:
     * @return:
     * @Date: 2020/10/17
     */
    int delete(String id);

    /**
     * @描述: 查询路由
     * @Param:
     * @return:
     * @Date: 2020/10/17
     */
    List<GatewayRouteDefinition> query();
}
