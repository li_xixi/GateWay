package com.rays.item.service.ex;

/**
 * @类描述: 服务异常
 * @author: Mr.Li
 * @create: 2020-05-11 15:53
 **/
public class ServerErrorExecption extends BaseResultException{
    public ServerErrorExecption(int status, String msg) {
        super(status, msg);
    }
}
