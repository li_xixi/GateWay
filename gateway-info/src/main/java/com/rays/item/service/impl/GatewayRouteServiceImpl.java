package com.rays.item.service.impl;

import com.rays.item.dao.GateRouteMapper;
import com.rays.item.pojo.GatewayFilterDefinition;
import com.rays.item.pojo.GatewayPredicateDefinition;
import com.rays.item.pojo.GatewayRouteDefinition;
import com.rays.item.service.GatewayRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @类描述: 实现类
 * @author: Mr.Li
 * @create: 2020-10-17 17:12
 **/
@Service
public class GatewayRouteServiceImpl implements GatewayRouteService {

    @Autowired
    GateRouteMapper gateRouteMapper;

    @Override
    public int add(GatewayRouteDefinition routeDefinition) {
        routeDefinition.setId(null);
        routeDefinition.setVersion(1);
        return gateRouteMapper.insert(routeDefinition);
    }

    @Override
    public int update(GatewayRouteDefinition routeDefinition) {
        //先将版本号查询出来
        GatewayRouteDefinition gatewayRouteDefinition = gateRouteMapper.selectByPrimaryKey(routeDefinition.getId());
        if (gatewayRouteDefinition != null){
            routeDefinition.setVersion(gatewayRouteDefinition.getVersion()+1);
            return gateRouteMapper.updateByPrimaryKey(routeDefinition);
        }
        return 0;
    }

    @Override
    public int delete(String id) {
       return gateRouteMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<GatewayRouteDefinition> query() {

        List<GatewayRouteDefinition> gatewayRouteDefinitions = gateRouteMapper.selectAll();

        for (GatewayRouteDefinition gatewayRouteDefinition : gatewayRouteDefinitions) {
            //组装参数
            List<GatewayPredicateDefinition> predicates = new ArrayList<>();
            String predicatesInfo = gatewayRouteDefinition.getPredicatesInfo();
            String[] infos = predicatesInfo.split("&");//Path=/api/item/**,/api/item&After=/api/test/**,/api/test
            for (String info : infos) {
                String[] split = info.split("=");//Path=/api/item/**,/api/item

                GatewayPredicateDefinition preDicatedefinition = new GatewayPredicateDefinition();

                Map<String, String> args = new LinkedHashMap<>();
                String[] split1 = split[1].split(",");
                for (int i = 0;i<split1.length;i++) {
                    args.put("_genkey_"+i,split1[i]);
                }

                preDicatedefinition.setName(split[0]);
                preDicatedefinition.setArgs(args);
                predicates.add(preDicatedefinition);
            }

            List<GatewayFilterDefinition> filters = new ArrayList<>();
            String filterInfo = gatewayRouteDefinition.getFiltersInfo();
            String[] filterInfos = filterInfo.split("&");//StripPrefix=2,3&RewritePath=/test/**,test/
            for (String info : filterInfos) {
                String[] split = info.split("=");//StripPrefix=2,3

                GatewayFilterDefinition filterDefinition = new GatewayFilterDefinition();

                Map<String, String> args = new LinkedHashMap<>();
                String[] split1 = split[1].split(",");
                for (int i = 0;i<split1.length;i++) {
                    args.put("_genkey_"+i,split1[i]);
                }
                filterDefinition.setName(split[0]);
                filterDefinition.setArgs(args);
                filters.add(filterDefinition);
            }

            gatewayRouteDefinition.setPredicatesInfo(null);
            gatewayRouteDefinition.setFiltersInfo(null);

            gatewayRouteDefinition.setPredicates(predicates);
            gatewayRouteDefinition.setFilters(filters);
        }
        return gatewayRouteDefinitions;
    }
}
