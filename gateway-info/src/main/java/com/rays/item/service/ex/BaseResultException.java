package com.rays.item.service.ex;

public abstract class BaseResultException extends RuntimeException{

    private int status;

    private String msg;

    public BaseResultException(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
