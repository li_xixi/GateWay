package com.rays.item.config;


import com.rays.item.interceptor.ResponseResultInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * 配置类
 * @author Administrator
 *
 */
@Configuration
public class InterceptorConfigurer implements WebMvcConfigurer {
	
	@Override					//注册工具
	public void addInterceptors(InterceptorRegistry registry) {
		//注册拦截器
		HandlerInterceptor interceptor = new ResponseResultInterceptor();
		List<String> patterns = new ArrayList<>();
		//patterns.add("/msgComm/upload");
		registry.addInterceptor(interceptor).
		//黑名单
		addPathPatterns("/**")
		//白名单
		.excludePathPatterns(patterns);
	}
}