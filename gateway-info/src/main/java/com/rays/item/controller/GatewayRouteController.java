package com.rays.item.controller;

import com.rays.item.annotation.ResponseResult;
import com.rays.item.pojo.GatewayRouteDefinition;
import com.rays.item.service.GatewayRouteService;
import com.rays.item.service.ex.BaseResultException;
import com.rays.item.service.ex.ServerErrorExecption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @类描述: 品牌管理
 * @author: Mr.Li
 * @create: 2020-05-09 22:17
 **/
@RestController
@RequestMapping("route")
@ResponseResult
public class GatewayRouteController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private GatewayRouteService service;

    /**
    * @描述: 新增路由信息
    * @Param: [routeDefinition]
    * @return: java.lang.Object
    * @Date: 2020/10/17
    */
    @PostMapping("/add")
    public Object add(@RequestBody GatewayRouteDefinition routeDefinition){
        try {
            return this.service.add(routeDefinition);
        }catch (Exception e){
          logger.error("Bad Things {}",e);
          if (e instanceof BaseResultException){
              return e;
          }else {
              return new ServerErrorExecption(500,"服务器异常...");
          }
        }
    }

    /**
     * @描述: 修改路由信息
     * @Param: [routeDefinition]
     * @return: java.lang.Object
     * @Date: 2020/10/17
     */
    @PostMapping("/update")
    public Object update(@RequestBody GatewayRouteDefinition routeDefinition){
        try {
            return this.service.update(routeDefinition);
        }catch (Exception e){
            logger.error("Bad Things {}",e);
            if (e instanceof BaseResultException){
                return e;
            }else {
                return new ServerErrorExecption(500,"服务器异常...");
            }
        }
    }

    /**
     * @描述: 删除路由信息
     * @Param: [routeDefinition]
     * @return: java.lang.Object
     * @Date: 2020/10/17
     */
    @GetMapping("/delete/{id}")
    public Object delete(@PathVariable("id") String id){
        try {
            return this.service.delete(id);
        }catch (Exception e){
            logger.error("Bad Things {}",e);
            if (e instanceof BaseResultException){
                return e;
            }else {
                return new ServerErrorExecption(500,"服务器异常...");
            }
        }
    }


    /**
     * @描述: 查询路由信息
     * @Param: [routeDefinition]
     * @return: java.lang.Object
     * @Date: 2020/10/17
     */
    @GetMapping("query")
    public Object query(){
        try {
            return this.service.query();
        }catch (Exception e){
            logger.error("Bad Things {}",e);
            if (e instanceof BaseResultException){
                return e;
            }else {
                return new ServerErrorExecption(500,"服务器异常...");
            }
        }
    }
}
