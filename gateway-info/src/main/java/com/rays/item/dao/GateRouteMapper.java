package com.rays.item.dao;

import com.rays.item.pojo.GatewayRouteDefinition;
import tk.mybatis.mapper.additional.idlist.SelectByIdListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @类描述: 品牌管理mapper
 * @author: Mr.Li
 * @create: 2020-05-09 22:30
 **/
public interface GateRouteMapper extends Mapper<GatewayRouteDefinition> , SelectByIdListMapper<GatewayRouteDefinition,Long>{

}
