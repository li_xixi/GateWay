package com.rays.gateway.service;

import com.rays.gateway.enity.GatewayFilterDefinition;
import com.rays.gateway.enity.GatewayPredicateDefinition;
import com.rays.gateway.enity.GatewayRouteDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @类描述: 动态路由service
 * @author: Mr.Li
 * @create: 2020-10-17 12:38
 **/
@Service
public class DynamicRouteServiceImpl implements ApplicationEventPublisherAware {

    @Autowired
    private RouteDefinitionWriter routeDefinitionWriter;

    private ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    /**
     * @描述: 添加加路由
     * @Param: [definition]
     * @return: java.lang.String
     * @Date: 2020/10/17
     */
    public void addRouter(GatewayRouteDefinition definition) {

        //保存路由信息
        routeDefinitionWriter.save(Mono.just(assembleRouteDefinition(definition))).subscribe();

        //发布路由更新的事件
        this.publisher.publishEvent(new RefreshRoutesEvent(this));
    }

    /**
     * @描述: 更新路由
     * @Param: [definition]
     * @return: java.lang.String
     * @Date: 2020/10/17
     */
    public void updateRouter(GatewayRouteDefinition definition) {
        //先删除路由信息
        this.deleteRouter(definition.getRouteId());
        //再发布新的路由信息
        routeDefinitionWriter.save(Mono.just(assembleRouteDefinition(definition))).subscribe();
        this.publisher.publishEvent(new RefreshRoutesEvent(this));
    }

    /**
     * @描述: 删除路由信息
     * @Param: [id]
     * @return: java.lang.String
     * @Date: 2020/10/17
     */
    public void deleteRouter(String id) {
        this.routeDefinitionWriter.delete(Mono.just(id)).subscribe();
    }

    /**
     * @描述: 组装 RouteDefinition
     * @Param: [gtwdefinition]
     * @return: org.springframework.cloud.gateway.route.RouteDefinition
     * @Date: 2020/10/17
     */
    private RouteDefinition assembleRouteDefinition(GatewayRouteDefinition gtwdefinition) {
        RouteDefinition definition = new RouteDefinition();
        List<PredicateDefinition> pdList = new ArrayList<>();
        definition.setId(gtwdefinition.getRouteId());
        List<GatewayPredicateDefinition> gatewayPredicateDefinitionList = gtwdefinition.getPredicates();
        for (GatewayPredicateDefinition gpDefinition : gatewayPredicateDefinitionList) {
            PredicateDefinition predicate = new PredicateDefinition();
            predicate.setArgs(gpDefinition.getArgs());
            predicate.setName(gpDefinition.getName());
            pdList.add(predicate);
        }

        List<GatewayFilterDefinition> gatewayFilterDefinitions = gtwdefinition.getFilters();
        List<FilterDefinition> filterList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(gatewayFilterDefinitions)) {
            for (GatewayFilterDefinition gatewayFilterDefinition : gatewayFilterDefinitions) {
                FilterDefinition filterDefinition = new FilterDefinition();
                filterDefinition.setName(gatewayFilterDefinition.getName());
                filterDefinition.setArgs(gatewayFilterDefinition.getArgs());
                filterList.add(filterDefinition);
            }
        }
        definition.setPredicates(pdList);
        definition.setFilters(filterList);
//        URI uri = UriComponentsBuilder.fromHttpUrl(gtwdefinition.getUri()).build().toUri();
        URI uri = URI.create(gtwdefinition.getUri());
        definition.setUri(uri);
        return definition;
    }
}
