package com.rays.gateway.feign;

import com.rays.gateway.enity.GatewayRouteDefinition;
import com.rays.gateway.utils.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("gateway-info")
public interface PullGatewayInfo {

    @GetMapping("/route/query")
    Response<List<GatewayRouteDefinition>> fetchRouteInfo();
}
