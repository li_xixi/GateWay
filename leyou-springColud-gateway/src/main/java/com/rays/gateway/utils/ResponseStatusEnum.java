package com.rays.gateway.utils;

/**
* @描述: 响应码
* @Param:
* @return:
* @Date: 2020/10/17
*/
public enum ResponseStatusEnum {

    ROUTER_ADD_SUCCESS(101,"路由添加成功"),

    ROUTER_ADD_FAILD(201,"路由添加失败"),

    ROUTER_UPDATE_SUCCESS(102,"路由更新成功"),

    ROUTER_UPDATE_FAILD(202,"路由更新失败"),

    ROUTER_DELETE_SUCCESS(103,"路由删除成功"),

    ROUTER_DELETE_FAILD(203,"路由删除失败");

    ResponseStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private Integer code;

    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
