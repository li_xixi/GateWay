package com.rays.gateway.utils;

/**
 * @类描述: ss
 * @author: Mr.Li
 * @create: 2020-10-17 13:10
 **/
public class ResponseBuilder<T> {

    private Integer code;
    private String msg;
    private T data;

    private ResponseBuilder() {

    }

    public static ResponseBuilder builder() {
        return new ResponseBuilder();
    }

    public ResponseBuilder withCode(Integer code) {
        this.code = code;
        return this;
    }

    public ResponseBuilder withMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public ResponseBuilder withData(T data) {
        this.data = data;
        return this;
    }

    public Response<T> build() {
        Response<T> response = new Response();
        response.setCode(this.code);
        response.setMsg(this.msg);
        response.setData(this.data);
        return response;
    }
}
