package com.rays.gateway.schedule;

import com.rays.gateway.enity.GatewayRouteDefinition;
import com.rays.gateway.feign.PullGatewayInfo;
import com.rays.gateway.service.DynamicRouteServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @类描述: 拉取路由信息
 * @author: Mr.Li
 * @create: 2020-10-17 16:01
 **/
@Service
public class PullRouteInfo {

    private static final Logger LOGGER = LoggerFactory.getLogger(PullRouteInfo.class);

    @Autowired
    PullGatewayInfo pullGatewayInfo;

    @Autowired
    DynamicRouteServiceImpl dynamicRouteService;

    //缓存路由信息
    private ConcurrentHashMap<String,Integer> versionMap = new ConcurrentHashMap();

    @PostConstruct
    public void initInfo(){
        try {
            List<GatewayRouteDefinition> gatewayRouteDefinition = pullGatewayInfo.fetchRouteInfo().getData();
            for (GatewayRouteDefinition routeDefinition : gatewayRouteDefinition) {
                dynamicRouteService.addRouter(routeDefinition);
                //保存id - version
                versionMap.put(routeDefinition.getRouteId(),routeDefinition.getVersion());
                LOGGER.info("add route:"+routeDefinition.getRouteId()+" success!!!");
            }
        }catch (Exception e){
            LOGGER.error("更新路由信息失败: {}",e);
        }
    }

    /**
    * @描述: 每十秒拉取一次数据库信息
    * @Param: []
    * @return: void
    * @Date: 2020/10/17
    */
    @Scheduled(cron = "5/10 * * * * ?")
    public void pullRouteInfo(){
        try {
            //调用维护服务的接口
            List<GatewayRouteDefinition> gatewayRouteDefinition = pullGatewayInfo.fetchRouteInfo().getData();
            //组装路由id
            List<Object> collect = gatewayRouteDefinition.stream().map(route ->route.getRouteId()).collect(Collectors.toList());
            for (String key : versionMap.keySet()) {
                if (!collect.contains(key)){
                    versionMap.remove(key);
                    //并且将路由信息移除
                    dynamicRouteService.deleteRouter(key);
                    LOGGER.info("移除路由:"+key);
                }
            }

            for (GatewayRouteDefinition routeDefinition : gatewayRouteDefinition) {
                if (versionMap.containsKey(routeDefinition.getRouteId())){
                    //包含该id 则判断是否修改过
                    if (versionMap.get(routeDefinition.getRouteId()) != routeDefinition.getVersion()){
                        //表示修改过，更新路由信息
                        dynamicRouteService.updateRouter(routeDefinition);
                        //修改缓存信息
                        versionMap.put(routeDefinition.getRouteId(),routeDefinition.getVersion());
                        LOGGER.info("update route:"+routeDefinition.getRouteId()+" success!!!");
                    }
                }else {
                    //不包含表示新建的路由，直接添加
                    dynamicRouteService.addRouter(routeDefinition);
                    //修改缓存信息
                    versionMap.put(routeDefinition.getRouteId(),routeDefinition.getVersion());
                    LOGGER.info("add route:"+routeDefinition.getRouteId()+" success!!!");
                }
            }
        }catch (Exception e){
            LOGGER.error("更新路由信息失败: {}",e);
        }
    }
}
