package com.rays.gateway.controller;

import com.rays.gateway.enity.GatewayRouteDefinition;
import com.rays.gateway.service.DynamicRouteServiceImpl;
import com.rays.gateway.utils.Response;
import com.rays.gateway.utils.ResponseBuilder;
import com.rays.gateway.utils.ResponseStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @类描述: 动态路由controller
 * @author: Mr.Li
 * @create: 2020-10-17 12:37
 **/
@RestController()
@RequestMapping("/route")
public class RouteController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouteController.class);

    @Autowired
    DynamicRouteServiceImpl dynamicRouteService;

    /**
    * @描述: 添加路由
    * @Param: [definition]
    * @return: com.rays.gateway.utils.Response<java.lang.String>
    * @Date: 2020/10/17
    */
    @PostMapping("/add")
    public Response<?> addRoute(@RequestBody GatewayRouteDefinition definition){
        Response<?> res;
        try {
            dynamicRouteService.addRouter(definition);
            res = ResponseBuilder.builder().withCode(ResponseStatusEnum.ROUTER_ADD_SUCCESS.getCode()).withMsg(ResponseStatusEnum.ROUTER_ADD_SUCCESS.getDesc()).build();
        }catch (Exception e){
            LOGGER.error("Bad Things {}",e);
            res = ResponseBuilder.builder().withCode(ResponseStatusEnum.ROUTER_ADD_FAILD.getCode()).withMsg(ResponseStatusEnum.ROUTER_ADD_FAILD.getDesc()).build();
        }
        return res;
    }

    /**
     * @描述: 更新路由
     * @Param: [definition]
     * @return: com.rays.gateway.utils.Response<java.lang.String>
     * @Date: 2020/10/17
     */
    @PostMapping("/update")
    public Response<?> updateRoute(@RequestBody GatewayRouteDefinition definition){
        Response<?> res;
        try {
            dynamicRouteService.updateRouter((definition));
            res = ResponseBuilder.builder().withCode(ResponseStatusEnum.ROUTER_UPDATE_SUCCESS.getCode()).withMsg(ResponseStatusEnum.ROUTER_UPDATE_SUCCESS.getDesc()).build();
        }catch (Exception e){
            LOGGER.error("Bad Things {}",e);
            res = ResponseBuilder.builder().withCode(ResponseStatusEnum.ROUTER_UPDATE_FAILD.getCode()).withMsg(ResponseStatusEnum.ROUTER_UPDATE_FAILD.getDesc()).build();
        }
        return res;
    }

    /**
     * @描述: 删除路由
     * @Param: [definition]
     * @return: com.rays.gateway.utils.Response<java.lang.String>
     * @Date: 2020/10/17
     */
    @GetMapping("/delete/{id}")
    public Response<?> deleteRoute(@PathVariable("id") String id){
        Response<?> res;
        try {
            dynamicRouteService.deleteRouter((id));
            res = ResponseBuilder.builder().withCode(ResponseStatusEnum.ROUTER_DELETE_SUCCESS.getCode()).withMsg(ResponseStatusEnum.ROUTER_DELETE_SUCCESS.getDesc()).build();
        }catch (Exception e){
            LOGGER.error("Bad Things {}",e);
            res = ResponseBuilder.builder().withCode(ResponseStatusEnum.ROUTER_DELETE_FAILD.getCode()).withMsg(ResponseStatusEnum.ROUTER_DELETE_FAILD.getDesc()).build();
        }
        return res;
    }
}
