# springCloud GateWay 动态设置路由

#### 介绍
两个服务，一个用于维护路由信息，一个Gateway定时拉取路由信息更新路由信息，这样的好处是我们实际生产环境可能会部署多个Gateway服务，我们只需要通过页面维护一次即可更新全部Gateway服务的路由信息


#### 使用说明

1.  分别修改两个服务的注册中心配置
2.  修改数据库gateway-info服务的数据库连接
3.  前端维护路由信息的页面还在开发中
4.  删除路由的方法网上很多是错的，少了subscribe()方法。代码里可以看到

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
