/*
Navicat MySQL Data Transfer

Source Server         : Lili
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : leyou

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2020-10-17 19:04:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gateway_dict`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_dict`;
CREATE TABLE `gateway_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '属性',
  `type` int(1) NOT NULL COMMENT '1:断言 2:过滤器',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='断言和过滤器的字典表';

-- ----------------------------
-- Records of gateway_dict
-- ----------------------------
