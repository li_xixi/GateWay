/*
Navicat MySQL Data Transfer

Source Server         : Lili
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : leyou

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2020-10-17 19:03:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gateway_info`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_info`;
CREATE TABLE `gateway_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `route_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '路由唯一id',
  `uri` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '转发目标路径',
  `order_info` int(11) NOT NULL COMMENT '排序',
  `filters_info` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '过滤器',
  `predicates_info` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '断言',
  `version` int(11) NOT NULL COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `route_id` (`route_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='SpringCloud Gateway 信息表';

-- ----------------------------
-- Records of gateway_info
-- ----------------------------
